My Module
====================================

DESCRIPTION
-----------
A block module that displays support information to site user.

FEATURES
-----------
- Creates a block to display support information to site user.

INSTALLATION
-----------
1) Copy module directory to site/all/modules/custom directory
2) Enable the module at: /admin/modules