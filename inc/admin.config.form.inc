<?php

/**
 * Implements hook_form().
 *
 * Admin form to configure support information
 */
function mymodule_admin_form($form, &$form_state) {
    $form['mymodule_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => variable_get("mymodule_name", ""),
        '#required' => TRUE,
    );
    $form['mymodule_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#default_value' => variable_get("mymodule_email", ""),
        '#required' => TRUE,
    );
    $form['mymodule_address'] = array(
        '#type' => 'textfield',
        '#title' => t('Address'),
        '#default_value' => variable_get("mymodule_address", ""),
        '#attributes' =>  array('pattern' => '.{20,}', 'title' => '20 characters minimum'),
        '#required' => TRUE,
    );


    //get country list
    $options = mymodule_get_country_list();

    $form['mymodule_country'] = array(
        '#type' => 'select',
        '#title' => t('Country'),
        '#options' => $options,
        '#default_value' => variable_get("mymodule_country", ""),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}

/**
 * Implements validation from the Form API.
 *
 * @param $form
 *   A structured array containing the elements and properties of the form.
 * @param $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function mymodule_admin_form_validate($form, &$form_state) {
    $email = $form_state['values']['mymodule_email'];
    //check email is valid
    if (!valid_email_address($email)) {
        form_set_error('mymodule_email', t('You must enter a valid email address.'));
    }

}